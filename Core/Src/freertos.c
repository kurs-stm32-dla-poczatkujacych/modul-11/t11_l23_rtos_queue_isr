/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usart.h"
#include "printf.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
typedef struct
{
	uint8_t Value;
	uint8_t Sender;
} Message_t;
/* USER CODE END Variables */
/* Definitions for Led2Task */
osThreadId_t Led2TaskHandle;
const osThreadAttr_t Led2Task_attributes = {
  .name = "Led2Task",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* Definitions for Producer1Task */
osThreadId_t Producer1TaskHandle;
const osThreadAttr_t Producer1Task_attributes = {
  .name = "Producer1Task",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 256 * 4
};
/* Definitions for Consumer1Task */
osThreadId_t Consumer1TaskHandle;
const osThreadAttr_t Consumer1Task_attributes = {
  .name = "Consumer1Task",
  .priority = (osPriority_t) osPriorityAboveNormal,
  .stack_size = 256 * 4
};
/* Definitions for Producer2Task */
osThreadId_t Producer2TaskHandle;
const osThreadAttr_t Producer2Task_attributes = {
  .name = "Producer2Task",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 256 * 4
};
/* Definitions for myQueue01 */
osMessageQueueId_t myQueue01Handle;
const osMessageQueueAttr_t myQueue01_attributes = {
  .name = "myQueue01"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartLed2Task(void *argument);
void StartProducer1(void *argument);
void StartConsumer1(void *argument);
void StartProducer2(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of myQueue01 */
  myQueue01Handle = osMessageQueueNew (16, sizeof(Message_t), &myQueue01_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of Led2Task */
  Led2TaskHandle = osThreadNew(StartLed2Task, NULL, &Led2Task_attributes);

  /* creation of Producer1Task */
  Producer1TaskHandle = osThreadNew(StartProducer1, NULL, &Producer1Task_attributes);

  /* creation of Consumer1Task */
  Consumer1TaskHandle = osThreadNew(StartConsumer1, NULL, &Consumer1Task_attributes);

  /* creation of Producer2Task */
  Producer2TaskHandle = osThreadNew(StartProducer2, NULL, &Producer2Task_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartLed2Task */
/**
  * @brief  Function implementing the Led2Task thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartLed2Task */
void StartLed2Task(void *argument)
{
  /* USER CODE BEGIN StartLed2Task */
  /* Infinite loop */
  for(;;)
  {
	  HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
	  printf("Led2\n\r");
	  osDelay(500);
  }
  /* USER CODE END StartLed2Task */
}

/* USER CODE BEGIN Header_StartProducer1 */
/**
* @brief Function implementing the Producer1Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartProducer1 */
void StartProducer1(void *argument)
{
  /* USER CODE BEGIN StartProducer1 */
	Message_t DataToSend = {0,1};
  /* Infinite loop */
  for(;;)
  {
	  if(osOK == osMessageQueuePut(myQueue01Handle, (Message_t*)&DataToSend, 0, osWaitForever))
	  {
		  printf("Producer1Task sent: %d\n\r", DataToSend.Value);
		  DataToSend.Value++;
	  }
	  osDelay(1000);
  }
  /* USER CODE END StartProducer1 */
}

/* USER CODE BEGIN Header_StartConsumer1 */
/**
* @brief Function implementing the Consumer1Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartConsumer1 */
void StartConsumer1(void *argument)
{
  /* USER CODE BEGIN StartConsumer1 */
	Message_t ReceivedData;
  /* Infinite loop */
  for(;;)
  {
	  if(osOK == osMessageQueueGet(myQueue01Handle, (Message_t*)&ReceivedData, NULL, osWaitForever))
	  {
		  printf("Consumer1Task received from %d: %d\n\r", ReceivedData.Sender, ReceivedData.Value);
	  }
  }
  /* USER CODE END StartConsumer1 */
}

/* USER CODE BEGIN Header_StartProducer2 */
/**
* @brief Function implementing the Producer2Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartProducer2 */
void StartProducer2(void *argument)
{
  /* USER CODE BEGIN StartProducer2 */
	Message_t DataToSend = {0,2};
  /* Infinite loop */
  for(;;)
  {
	  if(osOK == osMessageQueuePut(myQueue01Handle, (Message_t*)&DataToSend, 0, osWaitForever))
	  {
		  printf("Producer2Task sent: %d\n\r", DataToSend.Value);
		  DataToSend.Value++;
	  }
	  osDelay(500);
  }
  /* USER CODE END StartProducer2 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void _putchar(char character)
{
  // send char to console etc.
	HAL_UART_Transmit(&huart2, (uint8_t*)&character, 1, 1000);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	static Message_t DataToSend = {0,3};

	if(B1_Pin == GPIO_Pin)
	{
		  if(osOK == osMessageQueuePut(myQueue01Handle, (Message_t*)&DataToSend, 0, 0))
		  {
			  DataToSend.Value++;
		  }
	}
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
